import './App.css';
import Navbar from './Components/Navbar/Navbar';
import { BrowserRouter,Route,Routes } from 'react-router-dom';
import Home from './Pages/Home';
import Men from "./Pages/Men"
import Women from './Pages/Women';
import Product from './Pages/Product';
import Footer from './Components/Footer'


import Cart from './Pages/Cart';
import Accessories from './Pages/Accessories';
import LoginSignup from './Pages/LoginSignup';
import Register from './Pages/Register';


function App() {
  return (
    <div >
      <BrowserRouter>
      <Navbar />
      <Routes> 
        <Route path='/' element={<Home />}/> 
        <Route path='/men' element={<Men/>}/>
        <Route path='/women' element={<Women/>}/>
        <Route path='/accessories' element={<Accessories/>}/>
        <Route path='/product' element={<Product/>}/>
        <Route path='/cart' element={<Cart />}/>
        <Route path='/login' element={<LoginSignup />}/>
        <Route path='/register' element={<Register />}/>
      </Routes>
      <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
