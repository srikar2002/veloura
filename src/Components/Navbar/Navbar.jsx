import React, { useState } from 'react'
import './Navbar.css'

import cart_icon from '../Assets/Cart_icon.png'

import {Link} from 'react-router-dom'

const Navbar = () => {
  const[Menu,setMenu]=useState('home')
  return (
    <div className='Veloura'>
      <div className='navbar-main'>
      <nav className="navbar navbar-expand-lg navbar-light bg-light ">
            <div className="navbar-main-brand">veloura</div>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content:end" id="navbarNavAltMarkup">
              <div className="navbar-nav ml-auto">
                <a className="nav-main-link " href="#"><Link to={'/'} style={{textDecoration:"none",color:"black"}}>Home</Link></a>
                <a className="nav-main-link" href="#"><Link to={'/men'} style={{textDecoration:"none",color:"black"}}>Men</Link></a>
                <a className="nav-main-link" href="#"><Link to={'/women'} style={{textDecoration:"none",color:"black"}}>Women</Link></a>
                <a className="nav-main-link" href="#"><Link to={'/accessories'}  style={{textDecoration:"none",color:"black"}}>Accessiores</Link></a>
              </div>
            </div>
            <button className='nav-button'> <Link to={'/login'}  style={{textDecoration:"none",color:"black"}}>login</Link></button>
            <img className='nav-cart-icon' src={cart_icon} alt="" />
            <div className="nav-cart-count">0</div>
          </nav>
          </div> 
     
    </div>
    
  )
}

export default Navbar 