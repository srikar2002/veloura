import React, { useState } from 'react'
import {Input,FormGroup,Label,Form,Col,Row,Button} from 'reactstrap'
import axios  from 'axios'

function Register() {

  // let [email,setEmail] = useState("")
  // let [password,setPassword] = useState("")
  // let [address,setAddress] = useState("")

  let [formdata,setFormData] = useState({
    email:'',
    password:'',
    address:'',
    address2:'',
    city:'',
    check:'',
    state:''



  })
  const handleOnchange = (e) =>{
  //  let name = e.target.name
  //  let value = e.target.value


   let {name,value,type,checked} = e.target  //destructuring

   let inputValue = type === 'checkbox' ? checked : value

    setFormData({
     ...formdata,
       [name]:inputValue

     
     
    })
  
  }
  

  const handlesubmit = (e)=>{
    e.preventDefault();
    console.log(formdata)
    
     let register = async () =>{
      try{
        let response = await axios.post("http://localhost:3001/createUser",formdata)

        console.log(response)
      }catch(err){
        console.log(err)

      }


     }
     register()
  }

  return (
    <div style={{border:'1px solid gray', width:'500px',padding:'1rem',margin:'3rem auto',borderRadius:'5px'}}>
      <Form>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="Email"
          type="email"
          value= {formdata.email}
          onChange={handleOnchange}
          
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="password"
          placeholder="password"
          type="password"
          value = {formdata.password}
          onChange={handleOnchange}

        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup>
    <Label for="exampleAddress">
      Address
    </Label>
    <Input
      id="exampleAddress"
      name="address"
      placeholder="1234 Main St"
      value= {formdata.address}
      onChange={handleOnchange}

    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleAddress2">
      Address 2
    </Label>
    <Input
      id="exampleAddress2"
      name="address2"
      placeholder="Apartment, studio, or floor"
      value={formdata.address2}
      onChange={handleOnchange}
    />
  </FormGroup>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleCity">
          City
        </Label>
        <Input
          id="exampleCity"
          name="city"
          value={formdata.city}
          onChange={handleOnchange}
        />
      </FormGroup>
    </Col>
    <Col md={4}>
      <FormGroup>
        <Label for="exampleState">
          State
        </Label>
        <Input
          id="exampleState"
          name="state"
          value={formdata.state}
          onChange={handleOnchange}
        />
      </FormGroup>
    </Col>
    
  </Row>
  <FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
      value={formdata.check}
      onChange={handleOnchange}
    />
    <Label
      check
      for="exampleCheck"
    >
      Check me out
    </Label>
  </FormGroup>
  <Button   onClick={handlesubmit} type='submit'>
   Register
  </Button>
</Form>
    </div>
  )
}

export default Register