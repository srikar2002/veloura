import React, { useState } from 'react'
import Footer from '../Components/Footer'
import photo31_img from "../Components/Assets/Photo_31.png"
import photo32_img from "../Components/Assets/Photo_32.png"
import photo33_img from "../Components/Assets/Photo_33.png"
import photo34_img from "../Components/Assets/Photo_34.png"
import photo35_img from "../Components/Assets/Photo_35.png"
import photo36_img from "../Components/Assets/Photo_36.png"
import photo37_img from "../Components/Assets/Photo_37.png"
import photo38_img from "../Components/Assets/Photo_38.png"
import photo39_img from "../Components/Assets/Photo_39.png"
import photo40_img from "../Components/Assets/Photo_40.png"
import photo41_img from "../Components/Assets/Photo_41.png"
import photo42_img from "../Components/Assets/Photo_42.png"
import photo43_img from "../Components/Assets/Photo_43.png"
import photo44_img from "../Components/Assets/Photo_44.png"
import photo45_img from "../Components/Assets/Photo_45.png"



import { Card, CardBody, CardText, CardTitle, CardSubtitle, Button } from 'reactstrap'
const Accessories = () => {
    const [all_product, setall_product] = useState([
      {
        id: 31,
        name: "pink and black floral scrunchie",
        category: "accessories",
        image: photo31_img,
        new_price: 500,
        old_price: 960,
      },                                    
      {
        id: 32,
        name: "green floral scarf",
        category: "accessories",
        image: photo32_img,
        new_price: 1000,
        old_price: 1200,
      },                                    
      {
        id: 33,
        name: "JC lip gloss colour",
        category: "accessories",
        image: photo33_img,
        new_price: 2300,
        old_price: 3000,
      },                                    
      {
        id: 34,
        name: "rose passion edp 40ml",
        category: "accessories",
        image: photo34_img,
        new_price: 3800,
        old_price: 4000,
      },                                    
      {
        id: 35,
        name: "neverfull mm handbag",
        category: "accessories",
        image: photo35_img,
        new_price: 2500,
        old_price: 4000,
      },                                    
      {
        id: 36,
        name: "premium russian leather handbag",
        category: "accessories",
        image: photo36_img,
        new_price: 5600,
        old_price: 7000,
      },                                    
      {
        id: 37,
        name: "blue veloura barbie handbag",
        category: "accessories",
        image: photo37_img,
        new_price: 5500,
        old_price: 6000,
      },                                    
      {
        id: 38,
        name: "suede stripe oxford shoe",
        category: "accessories",
        image: photo38_img,
        new_price: 3800,
        old_price: 4000,
      },                                    
      {
        id: 39,
        name: "moccasin slippers",
        category: "accessories",
        image: photo39_img,
        new_price: 3800,
        old_price: 4000,
      },                                    
      {
        id: 40,
        name: "flag logo recatangle sunglasses",
        category: "accessories",
        image: photo40_img,
        new_price: 2800,
        old_price: 3000,
      },                                    
      {
        id: 41,
        name: "flat braided green bracelet",
        category: "accessories",
        image: photo41_img,
        new_price: 730,
        old_price: 850,
      },                                    
      {
        id: 42,
        name: "mixed chain link bracelet",
        category: "accessories",
        image: photo42_img,
        new_price: 750,
        old_price: 1200,
      },               
      {
        id: 43,
        name: "white cotton printed cap",
        category: "accessories",
        image: photo43_img,
        new_price: 1200,
        old_price: 1800,
      },                                    
      {
        id: 44,
        name: "VL tote pale white handbag",
        category: "accessories",
        image: photo44_img,
        new_price: 4000,
        old_price: 5000,
      },                                    
      {
        id: 45,
        name: "leather wallet men",
        category: "accessories",
        image: photo45_img,
        new_price: 1200,
        old_price: 1500,
      },                 
          
    ]);

    return (
        <div className='d-flex flex-wrap'>
            <div className='sub-class m-3 p-6 d-flex justify-content-between flex-wrap'  >

            
            {all_product.map((item)=>(<div>
                <Card
                    style={{
                        width: '18rem'
                    }}
                >
                    <img
                        alt="Sample"
                        src={item.image}
                    />
                    <CardBody>
                        <CardTitle tag="h5">
                            {item.name} <br/>
                            Price:{item.new_price}
                            
                        </CardTitle>
                        <CardSubtitle
                            className="mb-2 text-muted"
                            tag="h6"
                        >
                            category:{item.category} 
                            
                        </CardSubtitle>
                        <CardText>
                            Some quick example text to build on the card title and make up the bulk of the card‘s content. <br/>
                            
                        </CardText>
                        <Button>
                            Click Here
                        </Button>
                    </CardBody>
                </Card>
            </div>))}

        </div>
        </div>
    )
}

export default Accessories 