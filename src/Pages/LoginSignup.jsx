import React, { useState } from 'react'
import axios from 'axios';
import { Input, FormGroup, Label, Col, Form, Button } from 'reactstrap'
import {Link} from 'react-router-dom'
function LoginSignup() {
    let [formdata, setFormdata] = useState({
        email: '',
        password: '',
        check: false,
    
      })
      const handleOnChage = (e) => {
        let { name, value, type, checked } = e.target;
        let inputValue = type === 'checkbox' ? checked : value
        setFormdata({
          ...formdata,
          [name]: inputValue
        })
      }
      const handlesubmit = (e) => {
        e.preventDefault();
        console.log(formdata)
        login();
      }
    
      let login = async () => {
        try {
          let response = await axios.post("http://localhost:3001/login", formdata);
          console.log(response)
          console.log("Login successful");
        }
        catch (err) {
          console.log(err);
        }
      }

      return (
          <div style={{border:'1px solid gray', width:'600px',padding:'1rem',margin:'3rem auto',borderRadius:'5px'}}>
            <Form>
                <h3>LOGIN</h3>
                <Col md={6}>
                  <FormGroup>
                    <Label for="exampleEmail">
                      Email
                    </Label>
                    <Input
                      id="exampleEmail"
                      name="email"
                      placeholder="Enter Email"
                      type="email"
                      value={formdata.email}
                      onChange={handleOnChage}
                    />
                  </FormGroup>
                </Col>
                <Col md={6}>
                  <FormGroup>
                    <Label for="examplePassword">
                      Password
                    </Label>
                    <Input
                      id="examplePassword"
                      name="password"
                      placeholder=" Set Password"
                      type="password"
                      value={formdata.password}
                      onChange={handleOnChage}
                    />
                  </FormGroup>
                </Col>
              <FormGroup check>
                <Input
                  id="exampleCheck"
                  name="check"
                  type="checkbox"
                  value={formdata.checkbox}
                  onChange={handleOnChage}
                />
                <Label
                  check
                  for="exampleCheck"
                >
                  Check me out
                </Label>
              </FormGroup>
              <Button onClick={handlesubmit} type='submit'>
                Login
              </Button>
              <p><b><u>New User</u></b></p>
              <a className="Register_link " href="#"><Link to={'/register'} style={{textDecoration:'none', color:"black"}}>Register</Link></a>
            </Form>
          </div>
      )
}

export default LoginSignup