import React, { useState } from 'react'
import Footer from '../Components/Footer'
import photo1_img from "../Components/Assets/Photo_1.png"
import photo2_img from "../Components/Assets/Photo_2.png"
import photo3_img from "../Components/Assets/Photo_3.png"
import photo6_img from "../Components/Assets/Photo_6.png"
import photo7_img from "../Components/Assets/Photo_7.png"
import photo8_img from "../Components/Assets/Photo_8.png"
import photo9_img from "../Components/Assets/Photo_9.png"
import photo10_img from "../Components/Assets/Photo_10.png"
import photo11_img from "../Components/Assets/Photo_11.png"
import photo12_img from "../Components/Assets/Photo_12.png"
import photo13_img from "../Components/Assets/Photo_13.png"



import { Card, CardBody, CardText, CardTitle, CardSubtitle, Button } from 'reactstrap'
const Men = () => {
    const [all_product, setall_product] = useState([
        {
            id: 1,
            name: "slim fit dark brown shirt",
            category: "men",
            image: photo1_img,
            new_price: 5000.0,
            old_price: 3500.5,
          },
          {
            id: 2,
            name: "premimum white shirt",
            category: "men",
            image: photo2_img,
            new_price: 4000,
            old_price: 3000,
          },
          {
            id: 3,
            name: "lavender premimum shirt",
            category: "men",
            image: photo3_img,
            new_price: 2500,
            old_price: 3400,
          },
          {
            id: 4,
            name: "navy blue velora shirt",
            category: "men",
            image: photo6_img,
            new_price: 2800,
            old_price: 4000,
          },
          {
            id: 5,
            name: "pink 3d printed tee",
            category: "men",
            image: photo7_img,
            new_price: 2500,
            old_price: 3000,
          },
          {
            id: 6,
            name: "black party wear tshirt velora",
            category: "men",
            image: photo8_img,
            new_price: 2000,
            old_price: 2500,
          },
          {
            id: 7,
            name: "olive polo tshirt cardigian",
            category: "men",
            image: photo9_img,
            new_price: 1500,
            old_price: 2000,
          },
          {
            id: 8,
            name: "half polo lavender tshirt ",
            category: "men",
            image: photo10_img,
            new_price: 2100,
            old_price: 3000,
          },
          {
            id: 9,
            name: "washed black jeans ripped",
            category: "men",
            image: photo11_img,
            new_price: 3500,
            old_price: 4000,
          },
          {
            id: 10,
            name: "skinny light ripped jeans",
            category: "men",
            image: photo12_img,
            new_price: 2800,
            old_price: 3000,
          },
          {
            id: 11,
            name: "blue washed jeans men",
            category: "men",
            image: photo13_img,
            new_price: 2900,
            old_price: 5000,
          },
          
    ]);

    return (
        <div className='d-flex flex-wrap'>
            <div className='sub-class m-3 p-6 d-flex justify-content-between flex-wrap'  >

            
            {all_product.map((item)=>(<div>
                <Card
                    style={{
                        width: '18rem'
                    }}
                >
                    <img
                        alt="Sample"
                        src={item.image}
                    />
                    <CardBody>
                        <CardTitle tag="h5">
                            {item.name} <br/>
                            Price:{item.new_price}
                            
                        </CardTitle>
                        <CardSubtitle
                            className="mb-2 text-muted"
                            tag="h6"
                        >
                            category:{item.category} 
                            
                        </CardSubtitle>
                        <CardText>
                            Some quick example text to build on the card title and make up the bulk of the card‘s content. <br/>
                            
                        </CardText>
                        <Button>
                            Click Here
                        </Button>
                    </CardBody>
                </Card>
            </div>))}

        </div>
        </div>
    )
}

export default Men