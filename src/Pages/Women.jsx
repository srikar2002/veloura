import Footer from '../Components/Footer'
import React, { useState } from 'react'
import photo16_img from "../Components/Assets/Photo_16.png"
import photo17_img from "../Components/Assets/Photo_17.png"
import photo18_img from "../Components/Assets/Photo_18.png"
import photo19_img from "../Components/Assets/Photo_19.png"
import photo20_img from "../Components/Assets/Photo_20.png"
import photo21_img from "../Components/Assets/Photo_21.png"
import photo22_img from "../Components/Assets/Photo_22.png"
import photo23_img from "../Components/Assets/Photo_23.png"
import photo24_img from "../Components/Assets/Photo_24.png"
import photo25_img from "../Components/Assets/Photo_25.png"
import photo26_img from "../Components/Assets/Photo_26.png"
import photo27_img from "../Components/Assets/Photo_27.png"
import photo28_img from "../Components/Assets/Photo_28.png"
import photo29_img from "../Components/Assets/Photo_29.png"
import photo30_img from "../Components/Assets/Photo_30.png"
import { Card, CardBody, CardText, CardTitle, CardSubtitle, Button } from 'reactstrap'
const Women = () => {
  const [all_product, setall_product] = useState([
    {
        id: 16,
        name: "Striped Flutter Sleeve Overlap Collar Peplum Hem Blouse",
        category: "women",
        image: photo16_img,
        new_price: 50.0,
        old_price: 80.5,
      },                                    
      {
        id: 17,
        name: "tropical two tone resort midi dress",
        category: "women",
        image: photo17_img,
        new_price: 5000,
        old_price: 7000,
      },                                    
      {
        id: 18,
        name: "sparkling elegant knee-length party dress",
        category: "women",
        image: photo18_img,
        new_price: 2500,
        old_price: 5000,
      },                                    
      {
        id: 19,
        name: "cowl neck boxy sweater",
        category: "women",
        image: photo19_img,
        new_price: 1650,
        old_price: 1800,
      },                                    
      {
        id: 20,
        name: "dopamine satan pleaded trouser",
        category: "women",
        image: photo20_img,
        new_price: 2680,
        old_price: 3000,
      },                                    
      {
        id: 21,
        name: "tailored trouser with lined belt",
        category: "women",
        image: photo21_img,
        new_price: 3000,
        old_price: 4000,
      },                                    
      {
        id: 22,
        name: "elegant black short belted dress",
        category: "women",
        image: photo22_img,
        new_price: 2600,
        old_price: 3000,
      },                                    
      {
        id: 23,
        name: "boat neck fit and flare top",
        category: "women",
        image: photo23_img,
        new_price: 3600,
        old_price: 4000,
      },                                    
      {
        id: 24,
        name: "printed flare tube dress",
        category: "women",
        image: photo24_img,
        new_price: 2000,
        old_price: 2500,
      },                                    
      {
        id: 25,
        name: "v neck asymemntrical cutout detail dress",
        category: "women",
        image: photo25_img,
        new_price: 3800,
        old_price: 4000,
      },                                                                                                            
      {
        id: 26,
        name: "rhombus shaped printed scarf",
        category: "accessories",
        image: photo26_img,
        new_price: 1000,
        old_price: 1500,
      },                                    
      {
        id: 27,
        name: "cutout maxi body con dress",
        category: "women",
        image: photo27_img,
        new_price: 2800,
        old_price: 3000,
      },                                    
      {
        id: 28,
        name: "printed exclusive black dress",
        category: "women",
        image: photo28_img,
        new_price: 1500,
        old_price: 1800,
      },                                    
      {
        id: 29,
        name: "printed beach dress red",
        category: "women",
        image: photo29_img,
        new_price: 2000,
        old_price: 2300,
      },                                    
      {
        id: 30,
        name: "navy blue partywear one piece",
        category: "women",
        image: photo30_img,
        new_price: 2500,
        old_price: 1800,
      }, 
    
]);

return (
    <div className='d-flex flex-wrap'>
        <div className='sub-class m-3 p-6 d-flex justify-content-between flex-wrap'  >

        
        {all_product.map((item)=>(<div>
            <Card
                style={{
                    width: '18rem'
                }}
            >
                <img
                    alt="Sample"
                    src={item.image}
                />
                <CardBody>
                    <CardTitle tag="h5">
                        {item.name} <br/>
                        Price:{item.new_price}
                        
                    </CardTitle>
                    <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        category:{item.category} 
                        
                    </CardSubtitle>
                    <CardText>
                        Some quick example text to build on the card title and make up the bulk of the card‘s content. <br/>
                        
                    </CardText>
                    <Button>
                        Click Here
                    </Button>
                </CardBody>
            </Card>
        </div>))}

    </div>
    </div>
)

  
}

export default Women